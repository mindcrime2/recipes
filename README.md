# practice_1

Recipes SPA application
API: https://www.themealdb.com/

-random meals
-search by name
-search by ingredients
-search by letter

VueJS/ Vuex

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
