
import {createStore} from 'vuex'
import axiosClient from './axiosClient'
export default createStore({
    state:{
        randomMeals:[],
        tenRandomMeals:[],
        meals:[],
        searchMeals:[],
        mealsByLetter:[],
        mealsByIngredient:[],
        ingrediants:[],
        searchedIngrediants:[],
        loader:false
    },
    getters:{
        meals(state){
            return state.meals
        },
        tenRandomMeals(state){
            return state.tenRandomMeals
        },
        randomMeals(state){
            return state.randomMeals
        },
        ingrediants(state){
            return state.ingrediants
        },
        searchedIngrediants(state){
            return state.searchedIngrediants
        },
        getMealsByLetteer(state){
            return state.mealsByLetter
        },
        getSearchedMeals(state){
            return state.searchMeals
        },
        getSearchedMealsByIngredient(state){
            return state.mealsByIngredient
        },
        getLoader(state){
            return state.loader
        }
    },
    mutations:{
        resetMeals(state,payload){
            state[payload] = []
        },
        setMeals(state, payload){
            state.meals = payload
        },
        setTenRandomMeals(state, payload){
            state.tenRandomMeals.push(payload)
        },
        setRandomMeals(state,payload){
            state.randomMeals = payload
        },
        setIngrediants(state, payload){
            state.ingrediants = payload
        },
        setSearchedIngrediants(state,payload){
           
            state.searchedIngrediants = payload
        },
        setSearchMeals(state,payload){
            state.searchMeals = payload
        },
        setMealsByLetter(state,payload){
            state.mealsByLetter = payload
        },
        setSearchedMealsByingredient(state,payload){
            state.mealsByIngredient = payload
        },
        loadnigStarted(state){
            state.loader = true
        },
        loadingStoped(state){
            state.loader = false
        }
    },
    actions:{
        setSearchedIngrediants({commit,state},payload){
            let temp =  state.ingrediants.filter(ing=>ing.strIngredient.toLowerCase().includes(payload))
            commit('setSearchedIngrediants',temp)
        },
        async setMeals({commit}){
            const result = (await axiosClient.get('/list.php?i=list')).data
            
            commit('setMeals',result)
        },
        async setRandomMeals({commit}){
            try {
                commit('loadnigStarted')
                const result = (await axiosClient.get('/random.php')).data
               
                commit('setRandomMeals',result.meals)    
                commit('setTenRandomMeals', result.meals[0])
            } catch (error) {
                console.log(error)
            }finally{
                commit('loadingStoped')
            }
        },
        async setIngrediants({commit}){
            try {
                
                    commit('loadnigStarted')
                const result = (await axiosClient.get('/list.php?i=list')).data
                commit('setIngrediants',result.meals)
                commit('setSearchedIngrediants',result.meals)
            } catch (error) {
                console.log(error)
            }finally{
                
                    commit('loadingStoped')
            }
        },
        async setSearchMeals({commit},payload){
            try {
                commit('loadnigStarted')
                const result = (await axiosClient.get(`/search.php?s=${payload}`)).data
                commit('setSearchMeals',result.meals)
               
            } catch (error) {
                console.log(error)     
            }finally{
                commit('loadingStoped')
            }
        },
        async setMealsByLetter({commit}, payload){
            try {
                commit('loadnigStarted')
                const result = (await axiosClient.get(`/search.php?f=${payload}`)).data
                commit('setMealsByLetter', result.meals)
            } catch (error) {
                console.log(error)
            }finally{
                commit('loadingStoped')
            }
        },
        async setSearchedMealsByingredient({commit}, payload){
            try {
                commit('loadnigStarted')
                const result = (await axiosClient.get(`/filter.php?i=${payload}`)).data
                commit('setSearchedMealsByingredient', result.meals)
            } catch (error) {
                console.log(error)
            }finally{   
                commit('loadingStoped')
            }

        }
    }
})