import {createRouter,createWebHistory} from 'vue-router'
import SearchLinkResults from './components/SearchLinkResults.vue'
import SearchLinks from './components/SearchLinks'
import ByName from './components/ByName'
import ByIngrediants from './components/ByIngrediants'
import Home from './components/Home'
import DefaultLayout from './components/default/DefaultLayout'
import GuestLogin from './components/default/GuestLogin'
import MealDetalis from './components/MealDetails'
import MealsByIngrediants from './components/MealsByIngrediants'

export default createRouter({
    routes:[
        {path:'/',name:'default',component:DefaultLayout,children:[
            {path:'/',name:'home',component:Home},
        {path:'/byletter',name:'byletter',component:SearchLinks ,children:[
            {path:':letter?',name:'byletterresults',component:SearchLinkResults },
        ]},
        
        {path:'/by-name/:name?',name:'byname',component:ByName},
        {path:'/by-ingrediants',name:'byingrediants',component:ByIngrediants},
        {path:'/by-ingrediants/:nameId',name:'mealsbyingrediants',component:MealsByIngrediants},
        {path:'/meals/:id',name:'meal',component:MealDetalis}
        ]},
        {
            path:'/guest',name:'guest',component:GuestLogin,children:[
                
            ]
        }
        
    ],
    history:createWebHistory()
})